import logo from './logo.svg';
import './App.css';
import {ViewProperty,DeStructureProperty} from './components/functional/DeStructProp';
import {Filter,Map,Sort} from './components/functional/MapFilterSort';
import StateMagement from  './components/class/StateMangement';
import Counter,{CounterDuplicate} from './components/class/Counter';
import DeStructProp from './components/class/DeStructProp';
import {ClickEvents} from './components/class/Events';
import UserList from './components/functional/UserList'


function App() {
  return (
    <div className="App">

      {/* <div>===========================View and Destructure Property=======================</div>
      <ViewProperty name="Falgun" surname="Rajput" gender="Male"/>
      <DeStructureProperty name="Hiren" surname = "Patel" gender="Thummarr"></DeStructureProperty> */}

      {/* <div>============================Filter Map and Sorting====================================================</div>
      <Filter propName='name' propValue='Falgun'></Filter>
      <Filter propName='gender' propValue='Female'></Filter>
      <Filter propName='id' propValue='1'></Filter>
      <Filter propName='age' propValue='10'></Filter> */}

      {/* <Sort property="id" order="desc"></Sort> */}

      {/* <StateMagement name="Falgun"/> */}

      {/* <Counter/>
      <CounterDuplicate/>
      <DeStructProp name="Falgun"/>
      <DeStructProp name="Hiren"/> */}

      
      {/* <ClickEvents></ClickEvents> */}

      <UserList/>


    </div>
  );
}

export default App;
