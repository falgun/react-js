import React, { Component } from 'react'

export class ClickEvents extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            clickCount : 0,
            keyPressCount : 0

        }
        
    }

    increamentClickCount = () =>{
        this.setState({
            clickCount : this.state.clickCount + 1
        });
    }

    increamentkeyPressCount = () => {
        this.setState({
            keyPressCount : this.state.keyPressCount +1
        });
    }

    render() {
        return (
            <div>
                count : {this.state.clickCount} <br/>
                Key Press Count {this.state.keyPressCount} <br/>
                <button onClick={this.increamentClickCount}>Click Count</button>
                <input type='text' onKeyDown={this.increamentkeyPressCount}></input>
            </div>
        )
    }
}

