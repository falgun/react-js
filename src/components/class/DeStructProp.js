import React, { Component } from 'react'

export class DeStructProp extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             message : 'Hello ! Good Morning',
             gender : 'Male'
        }
    }
    
    render() {
        
        const {name} = this.props;
        const {message,gender} = this.state;

        return (
            <div>
                Hello {name} {message} You'r {gender}
            </div>
        )
    }
}

export default DeStructProp
