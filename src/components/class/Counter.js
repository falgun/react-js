import React, { Component } from 'react'

export class Counter extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             count : 0
        }
    }
    
    increament = () =>{
        this.setState((prevState,props)=>({
            count : prevState.count + 1
        }));
        console.log("Async Value",this.state.count);
    }


    increamentFive = () =>{
        this.increament();
        this.increament();
        this.increament();
        this.increament();
        this.increament();

    }

    render() {
        return (
            <div>
                Count : {this.state.count}
                <br/><button onClick={()=>this.increamentFive()}>Update Count</button>
            </div>
        )
    }
}

export class CounterDuplicate extends Component{

    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    

    render(){
        return (
            <div>This is the duplicate Counter Duplicate Method</div>
        );
    }

}

export default Counter
