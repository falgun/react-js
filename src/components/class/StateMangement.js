
import React,{Component} from 'react';

class StateManagement extends Component{

    constructor(){
        super();
        this.state = {
            message : 'Hello ! Welcome',
            color:'red'
        }
    }

    render(){
        return (
            <div>
                <div>{this.state.message},{this.props.name}</div>
                <button onClick={()=>this.changeMessage()}>Subscribe</button>
            </div>
        );
    }

    changeMessage = () =>{
       this.setState({
           message:'Hello ! Thank for subcribing'
       });
    }   

}

export default StateManagement