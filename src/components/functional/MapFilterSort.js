const users = [
    {'id':1,'name':'Falgun','gender':'Male','age':26},
    {'id':2,'name':'Mahir','gender':'Male','age':23},
    {'id':3,'name':'Jaimin','gender':'Female','age':10},
    {'id':4,'name':'Hiren','gender':'Male','age':40},
    {'id':5,'name':'Utsav','gender':'Male','age':89},
    {'id':6,'name':'Gautam','gender':'Female','age':35},
    {'id':7,'name':'Kushal','gender':'Male','age':85},
    {'id':8,'name':'Uresh','gender':'Female','age':2},
    {'id':9,'name':'Hemant','gender':'Male','age':8},
    {'id':10,'name':'Patil','gender':'Female','age':1}
]

export const Filter = ({propName,propValue}) =>{
    console.log(users.filter(user=>user[propName]==propValue));
    return 'check console log for filter functionality';
}

export const Sort = ({property,order}) => {
    
   users.sort((user1,user2)=>{
       return user1.age - user2.age;
   });

   console.log(users);

   return 'Function is sorted';
            
}