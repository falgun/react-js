import React from 'react'
import PersonDetailsRenderer from './PersonDetailsRenderer';

function UserList() {

    const users = [
        {
            'mobile':'8160232758',
            'name':'Falgun',
            'gender':'Falgun'

        },{
            'mobile':'9558833602',
            'name':'Himashu',
            'gender':'Jaimin'

        }
    
];
    const userList = users.map((user,index) => 
        <PersonDetailsRenderer   key={index} person = {user} />
    );

    return (<div>Here User List will render{userList}</div>);
}

export default UserList
