import React from 'react'
import  '../css/style.css';

function PersonDetailsRenderer({person}) {
    return (
        
        <div>
            {
                <h1 className='text-primary' id='btn'>Hello {person.name}. {person.gender},{person.mobile}</h1>
                
            }   
            <button className='button-primary'>This is the Button Demo</button> 
        </div>
    )
}

export default PersonDetailsRenderer
